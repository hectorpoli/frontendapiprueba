import { Component, OnInit } from '@angular/core';
import { MatSnackBar} from '@angular/material/snack-bar';
import {FormControl, Validators, ReactiveFormsModule} from '@angular/forms';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  username = new FormControl('', [Validators.required]);

  hide = true;
  constructor(public snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  openSnackBar() {
    this.snackBar.open('Hola, bienvenido', 'close', {
      duration: 2000,
    });
  }

  getErrorMessage() {
    return this.username.hasError('required') ? 'Debe ingresar un dato en el campo' :
            '';
  }

}
