import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientComponent } from './components/client/client.component';
import { CoreModule } from './core.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ClientComponent],
  imports: [
    CommonModule,
    CoreModule,
    ReactiveFormsModule,
  ],
  exports: [ClientComponent]
})
export class PagesModule { }
